import { getDate } from '../helpers/utils';

const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

const getData = async url => {
  const res = await fetch(url);
  if (!res.ok) {
    throw new Error(`Received ${res.status}`);
  }
  return await res.json();
};

const createMessages = data => {
  const messages = data.map(message => {
    const { id, text, userId, user, avatar, createdAt } = message;
    const [date, time] = getDate(createdAt);
    return { id, text, userId, user, avatar, date, time, isLike: false };
  });
  return messages;
};

const createUser = () => {
  return {
    user: 'Ben',
    avatar: 'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg',
    userId: '5328dba1-1b8f-11e8-9629-c7eca82aa7bd'
  };
};

const getMessages = async url => {
  const data = await getData(API_URL);
  const messages = createMessages(data);
  const currentUser = createUser();
  return { messages, currentUser };
};

export default getMessages;
