import React from 'react';
import PropTypes from 'prop-types';

import './message.css';

export const Message = props => {
  const { currentUserId, text, userId, user, avatar, time, isLike, onLike, onDelete, onEdit } = props;
  const isOwnMessage = currentUserId === userId;
  const messageClassNames = isOwnMessage ? 'message message--own' : 'message message--users';
  const likeClassNames = isLike ? 'message__like message__like--active' : 'message__like';

  return (
    <div className={messageClassNames}>
      <div className="message__body">
        {!isOwnMessage && <img className="message__avatar" src={avatar} alt={user} />}
        <div className="message__content">
          <p className="message__text">{text}</p>
          <p className="message__time">{time}</p>
        </div>
      </div>
      <div className="message__controls">
        <span className={likeClassNames}>
          <i className="fa fa-heart" aria-hidden="true" onClick={!isOwnMessage ? onLike : undefined} />
        </span>
        {isOwnMessage && (
          <>
            <span className="message__delete">
              <i className="fa fa-pencil" aria-hidden="true" onClick={onEdit}></i>
            </span>
            <span className="message__delete">
              <i className="fa fa-trash" aria-hidden="true" onClick={onDelete}></i>
            </span>
          </>
        )}
      </div>
    </div>
  );
};

Message.propTypes = {
  currentUserId: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  isLike: PropTypes.bool.isRequired,
  onLike: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired
};
