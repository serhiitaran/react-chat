import React, { Component } from 'react';

import getMessages from '../../services/message-service';
import { getIndex, getDate } from '../../helpers/utils';

import Loader from '../loader';
import Header from '../header';
import MessageList from '../message-list';
import MessageInput from '../message-input';

import './chat.css';

export class Chat extends Component {
  state = {
    isLoading: true,
    messages: [],
    currentUser: {},
    editMessage: { isEdit: false, id: null, text: null }
  };

  messageId = 0;

  componentDidMount() {
    getMessages().then(data => {
      this.setState({ messages: data.messages, currentUser: data.currentUser, isLoading: false });
    });
  }

  createMessage = (text, user) => {
    const id = this.messageId++;
    const isoDate = new Date().toISOString();
    const [date, time] = getDate(isoDate);
    return { id, text, date, time, ...user };
  };

  handleMessageLike = id => {
    this.setState(prevState => {
      const messageIndex = getIndex(prevState.messages, id);
      const oldMessage = prevState.messages[messageIndex];
      const newLikeValue = !oldMessage.isLike;
      const updatedMessage = { ...oldMessage, isLike: newLikeValue };
      const messages = [
        ...prevState.messages.slice(0, messageIndex),
        updatedMessage,
        ...prevState.messages.slice(messageIndex + 1)
      ];
      return { messages };
    });
  };

  handleMessageDelete = id => {
    this.setState(prevState => {
      const messageIndex = getIndex(prevState.messages, id);
      const messages = [...prevState.messages.slice(0, messageIndex), ...prevState.messages.slice(messageIndex + 1)];
      return { messages };
    });
  };

  handleMessageAdd = text => {
    this.setState(prevState => {
      const newMessage = this.createMessage(text, prevState.currentUser);
      const messages = [...prevState.messages, { ...newMessage }];
      return { messages };
    });
  };

  handleMessageEdit = ({ id, text }) => {
    this.setState({ editMessage: { isEdit: true, text, id } });
  };

  onMessageEdit = ({ isEdit, text, id }) => {
    this.setState(prevState => {
      if (!isEdit) {
        return { editMessage: { isEdit: false, text: null, id: null } };
      }
      const messageIndex = getIndex(prevState.messages, id);
      const oldMessage = prevState.messages[messageIndex];
      const updatedMessage = { ...oldMessage, text };
      const messages = [
        ...prevState.messages.slice(0, messageIndex),
        updatedMessage,
        ...prevState.messages.slice(messageIndex + 1)
      ];
      return { messages, editMessage: { isEdit: false, text: null, id: null } };
    });
  };

  render() {
    const { messages, isLoading, currentUser } = this.state;
    const chatName = 'React chat';
    const usersCount = new Set(messages.map(message => message.user)).size;
    const messagesCount = messages.length;
    const lastMessageTime = messages.length && messages.slice(-1)[0].time;

    return isLoading ? (
      <Loader />
    ) : (
      <div className="chat">
        <Header
          chatName={chatName}
          usersCount={usersCount}
          messagesCount={messagesCount}
          lastMessageTime={lastMessageTime}
        />
        <MessageList
          currentUserId={currentUser.userId}
          messages={messages}
          onMessageLike={this.handleMessageLike}
          onMessageDelete={this.handleMessageDelete}
          onMessageEdit={this.handleMessageEdit}
        />
        <MessageInput
          onMessageAdd={this.handleMessageAdd}
          onMessageEdit={this.onMessageEdit}
          editMessage={this.state.editMessage}
        />
      </div>
    );
  }
}
