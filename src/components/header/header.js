import React from 'react';
import PropTypes from 'prop-types';

import './header.css';

export const Header = ({ chatName, usersCount, messagesCount, lastMessageTime }) => {
  return (
    <header className="header">
      <h3 className="header__name">{chatName}</h3>
      <div className="header__users">
        <p>
          <i className="fa fa-user icon" />
          Users
        </p>
        <p>{usersCount}</p>
      </div>
      <div className="header__messages">
        <p>
          <i className="fa fa-comments icon" />
          Messages
        </p>
        <p>{messagesCount}</p>
      </div>
      <div className="header__time">
        <p>
          <i className="fa fa-clock-o icon" />
          Last message
        </p>
        <p>{lastMessageTime}</p>
      </div>
    </header>
  );
};

Header.propTypes = {
  headerInfo: PropTypes.shape({
    chatName: PropTypes.string.isRequired,
    usersCount: PropTypes.number.isRequired,
    messagesCount: PropTypes.number.isRequired,
    lastMessageTime: PropTypes.number.isRequired
  })
};
