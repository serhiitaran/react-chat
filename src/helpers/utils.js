export const getDate = isoDate => {
  const formatedDate = new Date(isoDate).toString();
  const date = formatedDate.substring(0, 10);
  const time = formatedDate.substring(15, 21);
  return [date, time];
};

export const getIndex = (items, id) => {
  return items.findIndex(item => item.id === id);
};
