import React from 'react'
import ReactDOM from 'react-dom'

import Chat from './components/chat'

import './index.css'

ReactDOM.render(<Chat />, document.getElementById('root'))
